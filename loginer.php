<?php
    declare(strict_types = 1);

    define("ROOT_PATH", dirname(__FILE__));

    //**************************************************************
    //Functions
    
    //Function for check login and password
    function checkLoginPsw(string $fileName, string $login, string $psw): array
    {
        $checkBool [0] = false;
        $checkBool [1] = false;
        $i = 0;

        $linesLog = [];
        $lineArr = [];
        $registeredUsers = [];

        $logFile = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . $fileName, "r");

    if ($logFile) {
        while (($linesLog[] = fgets($logFile, 100)) !== false) {
            //Deleting line translation carriage return and other symbols
            $linesLog[$i] = trim($linesLog[$i], " \t\n\r\0\x0B");
            //Exploding read lines into words and writing it to array
            $lineArr = explode(" ", $linesLog[$i]);
            $registeredUsers["login"][$i] = $lineArr[0];
            $registeredUsers["password"][$i] = $lineArr[1];
            $i++;
        }
        if (!feof($logFile)) {
            echo "Error: fgets() suddenly failed<br>";
            die();
        }
        fclose($logFile);
    }

        $i = 0;
        foreach ($registeredUsers["login"] as $log) {
            if ($login == $log) {//if $login is proper
                $checkBool [0] = true;
                //Checking the password
                if ($psw == $registeredUsers["password"][$i]) {
                    $checkBool [1] = true;
                }
                break;
            }
            $i++;
        }
        return $checkBool;
    }

    //Function for creating and writing statistic file which contents numbers of correct and incorrect accesses
    function accessStatistics (string $Login, bool $correct) {
        $i = 0;
        $temp;
        $lines = [];
        $lineArr = [];
        $textFile = "";

        $userFileAddress = ROOT_PATH . DIRECTORY_SEPARATOR . $Login . ".txt";
        //Creating or opening exists file
        if (! file_exists($userFileAddress)) {//If file isn't exists
            //Creating empty blank of file
            $blankFile = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "user_blank.txt", "r");
            while (($lines[$i] = fgets($blankFile, 100)) !== false) {
                $i++;
            }
            fclose($blankFile);//Closing blank file
            
        } else { //If file exists
            $userFile = fopen($userFileAddress, "r");
            while (($lines[$i] = fgets($userFile, 100)) !== false) {
                $i++;
            }
            fclose($userFile);
        }
        $userFile = fopen($userFileAddress, "w+");
        if ($correct == true) {//if user did correct enter
            $i = 0;
        } else { //if user didn't correct enter
            $i = 1;
        }
        //Deleting line translation and carriage return symbols
        $lines[$i] = trim($lines[$i], " \t\n\r\0\x0B");
        //Exploding readed line into words and writes it to array
        $lineArr = explode(" ", $lines[$i]);                
        $temp = (int) end($lineArr);
        $temp++;
        $lineArr [array_key_last ($lineArr)] = (string) $temp;
        $lines[$i] = implode(" ", $lineArr);
        $lines[$i] .= "\r\n";
        //Merging all strings
        foreach ($lines as $line) {
            $textFile .= $line;
        }
        //Writing $textFile into the $Login . "txt"
        fwrite($userFile, $textFile);
        fclose($userFile);
    }

    //************************************************************
    //Main cycle

    //Getting $Login and $Psw from HTML-form
    $Login = $_GET["login"];
    $Psw = $_GET["psw"];
    
	$checkBool = checkLoginPsw("Registered_users.txt", $Login, $Psw);		
	if ($checkBool [0] == true){
		//If user did correct enter his login
		if ($checkBool [1] == true) {
			//If user did correct enter his password
			echo "Login: " . $Login . "<br>";
			accessStatistics ($Login, true);
		} else {
			echo "Incorrect entering password<br>";
			accessStatistics ($Login, false);
		}
		echo "<pre>";
		var_dump(file(ROOT_PATH . DIRECTORY_SEPARATOR . $Login . ".txt"));
		echo "</pre>";
	} else {
		echo "User with this login isn't exists<br>";
	}

?>